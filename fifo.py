from operator import itemgetter
from plotly.figure_factory import create_gantt

numero_procesos = int(input("Ingrese el número de procesos: "))

# Pedir procesos, rafagas y tiempos
procesos = []
for i in range(numero_procesos):
  proceso = []
  print("\n", "Proceso número", i+1)

  nombre = input("Inserte nombre del proceso: ")
  proceso.append(nombre)

  rafaga = int(input("Inserte rafaga del proceso: "))
  proceso.append(rafaga)

  tiempo = int(input("Inserte tiempo del proceso: "))
  proceso.append(tiempo)
  
  procesos.append(proceso)

# Ordenar procesos segun el algoritmo FIFO, Esto se hace según el tiempo de llegada.
procesos_ordenados = sorted(procesos, key=itemgetter(2))

# Calcular el tiempo de espera individual y general de los procesos.
tiempos_salidas = []
tiempo_salida = procesos_ordenados[0][2]
tiempo_espera = 0
for i in range(numero_procesos):
  tiempos_salidas.append(tiempo_salida)
  tiempo_llegada = procesos_ordenados[i][2]
  tiempo_proceso = tiempo_salida - tiempo_llegada
  tiempo_salida += procesos_ordenados[i][1]
  tiempo_espera += tiempo_proceso

tiempo_espera /= numero_procesos
print("\nTiempo de espera: ", tiempo_espera)

# Calcular el tiempo de sistema individual y general de los procesos.
tiempos_finales = []
tiempo_final = procesos_ordenados[0][2]
tiempo_sistema = 0
for i in range(numero_procesos):
  tiempo_llegada = procesos_ordenados[i][2]
  tiempo_final += procesos_ordenados[i][1]
  tiempo_proceso = tiempo_final - tiempo_llegada
  tiempo_sistema += tiempo_proceso
  tiempos_finales.append(tiempo_final)

tiempo_sistema /= numero_procesos
print("Tiempo de sistema: ", tiempo_sistema)

# Realizar el diagrama Grantt del algoritmo FIFO, Esto se hace segun los tiempos de salida y tiempos finales.
tiempos = []
for i in range(numero_procesos):
  tiempos.append([procesos_ordenados[i][0], tiempos_salidas[i], tiempos_finales[i]])

datos = []
for i in range(numero_procesos):
  dato = dict(Task=tiempos[i][0], Start=tiempos[i][1], Finish=tiempos[i][2])
  datos.append(dato)

diagrama = create_gantt(df=datos, title="Diagrama Grantt del Algoritmo de Despacho FIFO")
diagrama.update_xaxes(type="linear")
diagrama.show()